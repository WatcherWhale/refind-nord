# rEFInd Nord theme

A simplistic clean and minimal theme for rEFInd in the Nord Theme

 **press F10 to take screenshot**

### Installation [Manual]:

1. Clone git repository to your $HOME directory.
   ```
   git clone https://gitlab.com/WatcherWhale/refind-nord.git refind-nord
   ```

2. Remove unused directories and files.
   ```
   sudo rm -rf refind-nord/{src,.git}
   ```
   ```
   sudo rm Refind-Nord/install.sh
   ```

3. Locate refind directory under EFI partition. For most Linux based system is commonly `/boot/efi/EFI/refind/`. Copy theme directory to it.

   **Important:** Delete older installed versions of this theme before you proceed any further.

   ```
   sudo rm -rf /boot/efi/EFI/refind/refind-nord
   ```
   ```
   sudo cp -r refind-nord /boot/efi/EFI/refind/
   ```

4. To adjust icon size, font size, background color and selector color edit `theme.conf`.
   ```
   sudo vi /boot/efi/EFI/refind/refind-nord/theme.conf
   ```

5. To enable the theme add `include refind-nord/theme.conf` at the end of `refind.conf`, and comment out or delete any other themes you might have installed.
   ```
   sudo vi /boot/efi/EFI/refind/refind.conf

   ```
